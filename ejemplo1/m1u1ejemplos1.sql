﻿DROP DATABASE IF EXISTS m1u1ejemplos1;
CREATE DATABASE m1u1ejemplos1;
USE m1u1ejemplos1;

CREATE OR REPLACE TABLE productos(
  idProducto int,
  nombre varchar(20),
  peso float, 
  PRIMARY KEY (idProducto)
);

CREATE OR REPLACE TABLE clientes(
  idCliente int,
  nombre varchar(20),
  PRIMARY KEY (idCliente)
  );

CREATE OR REPLACE TABLE compran(
  producto int,
  cliente int,
  fecha date, 
  cantidad int,
  PRIMARY KEY (producto, cliente),
  CONSTRAINT fkcompran_productos FOREIGN KEY (producto) REFERENCES productos (idProducto),
  CONSTRAINT fkcompran_clientes FOREIGN KEY (cliente) REFERENCES clientes (idCliente)
);

/* introducción de los datos a partir de la backup de los importados
  He cambiado la fecha de compran a tipo datetime en el editor, daría error si meto fecha y hora sólo con date */

INSERT INTO productos VALUES
(1, 'PRODUCTO 1', 2),
(2, 'PRODUCTO 2', 3),
(3, 'PRODUCTO 3', 4),
(4, 'PRODUCTO 4', 5),
(5, 'PRODUCTO 5', 6),
(6, 'PRODUCTO 6', 7),
(7, 'PRODUCTO 7', 8),
(8, 'PRODUCTO 8', 9),
(9, 'PRODUCTO 9', 10),
(10, 'PRODUCTO 10', 11),
(11, 'PRODUCTO 11', 12),
(12, 'PRODUCTO 12', 13),
(13, 'PRODUCTO 13', 14),
(14, 'PRODUCTO 14', 15),
(15, 'PRODUCTO 15', 16),
(16, 'PRODUCTO 16', 17),
(17, 'PRODUCTO 17', 18),
(18, 'PRODUCTO 18', 19),
(19, 'PRODUCTO 19', 20);


INSERT INTO clientes VALUES
(1, 'CLIENTE 1'),
(2, 'CLIENTE 2'),
(3, 'CLIENTE 3'),
(4, 'CLIENTE 4'),
(5, 'CLIENTE 5'),
(6, 'CLIENTE 6'),
(7, 'CLIENTE 7'),
(8, 'CLIENTE 8'),
(9, 'CLIENTE 9'),
(10, 'CLIENTE 10'),
(11, 'CLIENTE 11'),
(12, 'CLIENTE 12'),
(13, 'CLIENTE 13'),
(14, 'CLIENTE 14'),
(15, 'CLIENTE 15'),
(16, 'CLIENTE 16'),
(17, 'CLIENTE 17'),
(18, 'CLIENTE 18'),
(19, 'CLIENTE 19'),
(20, 'CLIENTE 20');


INSERT INTO compran VALUES
(1, 18, '2019-06-17 00:00:00', NULL),
(1, 19, '2019-06-18 00:00:00', NULL),
(2, 18, '2019-06-20 00:00:00', NULL),
(3, 1, '2019-06-04 00:00:00', NULL);
