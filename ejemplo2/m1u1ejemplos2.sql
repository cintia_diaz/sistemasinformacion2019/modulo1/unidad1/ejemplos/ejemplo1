﻿DROP DATABASE IF EXISTS m1u1ejemplos2;
CREATE DATABASE m1u1ejemplos2;
USE m1u1ejemplos2;


CREATE OR REPLACE TABLE clientes(
  idCliente int,
  nombre varchar(20),
  PRIMARY KEY (idCliente)
  );


CREATE OR REPLACE TABLE productos(
  idProducto int,
  nombre varchar(20),
  peso float,
  cliente int,
  fecha date,
  cantidad int,
  PRIMARY KEY (idProducto),
  CONSTRAINT fkproductos_cliente FOREIGN KEY (cliente) REFERENCES clientes (idCliente)
  );