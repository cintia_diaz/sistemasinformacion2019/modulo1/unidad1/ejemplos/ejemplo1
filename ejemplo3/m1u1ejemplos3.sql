﻿DROP DATABASE IF EXISTS m1u1ejemplos3;
CREATE DATABASE m1u1ejemplos3;
USE m1u1ejemplos3;

CREATE OR REPLACE TABLE productos(
  idProducto int,
  nombre varchar(20),
  peso float, 
  PRIMARY KEY (idProducto)
);

CREATE OR REPLACE TABLE clientes(
  idCliente int,
  nombre varchar(20),
  PRIMARY KEY (idCliente)
  );

CREATE OR REPLACE TABLE compran(
  producto int,
  cliente int,
  fecha date, 
  cantidad int,
  PRIMARY KEY (producto, cliente),
  UNIQUE KEY (producto),
  UNIQUE KEY (cliente),
  CONSTRAINT fkcompran_productos FOREIGN KEY (producto) REFERENCES productos (idProducto),
  CONSTRAINT fkcompran_clientes FOREIGN KEY (cliente) REFERENCES clientes (idCliente)
);



