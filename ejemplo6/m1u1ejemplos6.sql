﻿DROP DATABASE IF EXISTS m1u1ejemplos6;
CREATE DATABASE m1u1ejemplos6;
USE m1u1ejemplos6;

CREATE OR REPLACE TABLE productos(
  idProducto int,
  nombre varchar(20),
  peso float, 
  PRIMARY KEY (idProducto)
);

CREATE OR REPLACE TABLE clientes(
  idCliente int,
  nombre varchar(20),
  PRIMARY KEY (idCliente)
  );

CREATE OR REPLACE TABLE telefonoClientes(
  cliente int,
  telefono varchar(10),
  PRIMARY KEY (cliente, telefono),
  CONSTRAINT fktelefono_cliente FOREIGN KEY (cliente) REFERENCES clientes (idCliente)
  );

CREATE OR REPLACE TABLE tienda(
  codigo int,
  direccion varchar(20),
  PRIMARY KEY (codigo)
);

CREATE OR REPLACE TABLE compran(
  producto int,
  cliente int,
  tienda int,
  fecha date, 
  cantidad int,
  PRIMARY KEY (producto, cliente, tienda), 
  UNIQUE KEY (producto, tienda),
  CONSTRAINT fkcompran_productos FOREIGN KEY (producto) REFERENCES productos (idProducto),
  CONSTRAINT fkcompran_clientes FOREIGN KEY (cliente) REFERENCES clientes (idCliente),
  CONSTRAINT fkcompran_tienda FOREIGN KEY (tienda) REFERENCES tienda (codigo)
);

